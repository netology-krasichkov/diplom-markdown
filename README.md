# Дипломный практикум в Yandex.Cloud

**Исполнитель: Красичков Александр Владимирович**

## Этапы выполнения:

### Подготовка

**Организация пространства**

Для выполнения дипломной работы будет использоваться облако **cloud-s7ade**. Создана отдельная директория **diplom**.
Сервисный аккаунт **sa-terraform** имеет доступ к folder **diplom** как **editor**, что дает ему полные права доступа к рабочей директории, но не позволяет создавать новых пользователей. Нам понадобится в дальнейшем создавать дополнительные сервисные аккаунты (например для бакета), в связи с чем добавляем роль **organization-manager.admin**

**Устанавливаем Terraform 1.5.7 как дополнительный**

```sh
stade@stade-B760M-AORUS-ELITE-AX:~/Загрузки$ terraform1.5 -v
Terraform v1.5.7
```


### Создание облачной инфраструктуры

___
#### Подготовьте бэкенд для Terraform

Был создан Бакет S3 в созданном ЯО аккаунте с помощью Terraform.

Создан дополнительный сервисный аккаунт **"sa-bucket"**, выделены права **"storage.editor"**. Название бакета - **"stadeoff-s3"**

[bucket.tf](./files/terraform/bucket.tf)

**Git Repo:** https://gitlab.com/netology-krasichkov/terraform-struct/backend
___

#### Создайте VPC с подсетями в разных зонах доступности.

Была создана сеть **netology_vpc**.

Данная сеть включает в себя 3 подсети:

|ru-central1-a|ru-central1-b|ru-central1-d
|---|---|---
|subnet-a|subnet-b|subnet-d|
|192.168.10.0/24|192.168.20.0/24|192.168.30.0/24|

[vpc.tf](./files/terraform/vpc.tf)

**Git Repo:** https://gitlab.com/netology-krasichkov/terraform-struct/vpc

**Демонстрация доступности сервисов на текущий момент:**

![Промежуточное состояние](./files/img/state1.png)

___

### Создание кластера Kubernetes

Для выполнения дипломный работы был выбран вариант создание Kubernetes кластера с помощью **Yandex Managed Service for Kubernetes**. 

На этапе написания конфигурации столкнулся с проблемой передачи переменных из разных директорий. Мной было принято решение о использовании **output** из сохраненных состояний Terraform в **s3 bucket**. 

Выделил отдельную директорию **sa-list**, которая предназначена для сервисных аккаунтов, которые могут пригодится в ходе выполнения дипломной работы. На данном этапе был создан сервисный аккаунт **sa-k8s**, который будет использоваться для управления кластером.
Ему был выдан необходимый минимум ролей: 
| | | sa-k8s| |
|---|---|---|---|
|vpc.publicAdmin| k8s.clusters.agent| container-registry.images.puller| kms.keys.encrypterDecrypter| 
___

[sa-list.tf](./files/terraform/sa-list.tf)

**Git Repo:** https://gitlab.com/netology-krasichkov/terraform-struct/sa-list 
___
Подключил к кластеру логирование **Yandex.Cloud**. Развертывание **регионального мастера** было произведено без ошибок.


```
yandex_kubernetes_cluster.stade-cluster-regional: Creation complete after 5m16s [id=cat6sl3ejfnb6s3i34ic]

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.
```

[kub-master.tf](./files/terraform/kub-master.tf)

**Git Repo:** https://gitlab.com/netology-krasichkov/terraform-struct/kub-master

### Cоздание node group

Для создания worker node мной был использован ресурс **"yandex_kubernetes_node_group"**. Для их размещения использую 3 ранее созданные подсети, в разных зонах доступности.

**Ресурсы**

Для каждой worker node были выделены 2gb ОЗУ, 2 CPU, 64gb HDD. Так как мы ограничены в бюджете, процессорное время было установлено на 50%, машины прерываемые.

[kube-node-group.tf](./files/terraform/kube-node-group.tf)

**Git Repo:** https://gitlab.com/netology-krasichkov/terraform-struct/kube-node-group

```kubectl get nodes:```

![Get nodes](./files/img/get-nodes.png)

``kubectl get pods --all-namespaces:``

![Get pods](./files/img/get-pods.png)

**Состояние на текущий момент времени:**

![Состояние системы](./files/img/state2.png)

___

### Создание тестового приложения

Для создания тестового приложения буду использовать HTML/CSS вёрстку, созданную мной.

Для создания образа основанном на nginx, отдающем статичные файлы, мной написан простой **Dockerfile:**

```Docker
FROM nginx:alpine

COPY . /usr/share/nginx/html
```
``docker run -tid -p 80:80 --name static-html static-img``

Проверяем валидность конфигурации на локальной машине по localhost:80:

![Статичная html](./files/img/static-page.png)

В качестве Container Registry использую **hub.docker.com**.

```docker push stadeoff/static-page```

**Docker Image:** https://hub.docker.com/r/stadeoff/static-page

**Git Repo:** https://gitlab.com/netology-krasichkov/static-page

____

### Подготовка cистемы мониторинга и деплой приложения

В первом этапе, мной было принято решение выполнения пункта 3. Мной был настроен автоматический запуск и применение конфигурации "Terraform" из git-репозитория. 

Он включает следующие **stages:**

1. Validate - проверка валидности конфигурации.
2. Plan - создание файла, включающий предварительные изменения.
3. Apply - применение конфигурации "Terraform". Запуск в ручном режиме.
4. Destroy - удаление ресурсов "Terraform". Запуск при условии ${DESTROY} == 'true'

Созданный pipeline добавлен ко всем Terraform-ресурсам и выполняется успешно.

![Success apply](./files/img/succ_apply.png)

![Success destroy](./files/img/succ_destroy.png)

![Success logs](./files/img/succ_logs.png)

[.gitlab-ci.yml](./files/ci/terraform.gitlab-ci.yml)

___

### Деплой системы мониторинга

Для развертывания системы мониторинга я использовал .gitlab-ci и репозиторий [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus). Мной был создан ${SECRET_KEY} для использования в переменных pipeline.

![Success Grafana deployment](./files/img/success_grafana.png)

![Grafana dashboard](./files/img/grafana.png)

Доступна по ip: http://158.160.153.239/

**Git Repo:** https://gitlab.com/netology-krasichkov/grafana-kub

[.gitlab-ci.yml](./files/ci/grafana.gitlab-ci.yml)

### Установка и настройка CI/CD

Мной была написана простая конструкция, которая позволяет помечать сбилженный образ без тегов **${CI_COMMIT_SHORT_SHA}**, а если тег присутствует, то использовать его. 

```yml
build:
   stage: build
   tags:
     - ${DOCKER_TAG}
   image: docker:20.10
   variables:
     DOCKER_HOST: tcp://docker:2375/
     DOCKER_DRIVER: overlay2
     DOCKER_TLS_CERTDIR: ""
     GIT_SSL_NO_VERIFY: "1"
   services:
     - name: docker:dind
       alias: docker
   before_script:
     - docker login -u ${CI_DOCKER_USER} -p ${CI_DOCKER_PASS}
   script:
     - |
      if [[ ! -z "${CI_COMMIT_TAG}" ]]; then
          docker build -t stadeoff/static-page:${CI_COMMIT_TAG} .
          docker push stadeoff/static-page:${CI_COMMIT_TAG}
      else
          docker build -t stadeoff/static-page:${CI_COMMIT_SHORT_SHA} .
          docker push stadeoff/static-page:${CI_COMMIT_SHORT_SHA}
      fi
```
![Tags](./files/img/tags.png)

Для деплоя в кластер буду использовать Helm.

![Tag deploy](./files/img/tag_deploy.png)

Приложение доступно по адресу http://158.160.157.97

**HelmChart:** https://gitlab.com/netology-krasichkov/static-page/-/tree/main/HelmChart?ref_type=heads

**Успешный деплой с тегом:** https://gitlab.com/netology-krasichkov/static-page/-/pipelines/1225076790

**Git Repo:** https://gitlab.com/netology-krasichkov/static-page