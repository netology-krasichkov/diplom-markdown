### Указываем яндекс провайдера и s3 backend

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoint = "https://storage.yandexcloud.net"
    bucket = "stadeoff-s3"
    region = "ru-central1"
    key    = "kube-node-group/terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  zone = "ru-central1-a"                                                    ## Зона доступности по умолчанию
  service_account_key_file = file("./key.json")
  folder_id = var.folder_id
}
################################

### Cоздаем node группу "stade-node-group"

resource "yandex_kubernetes_node_group" "stade-node-group" {
  cluster_id  = data.terraform_remote_state.kub-master.outputs.stade-cluster-id
  name        = "stade-node-group"
  description = "Node group for netology diplom"

  labels = {
    "type" = "node-group"
  }

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat                = true
      subnet_ids         = [data.terraform_remote_state.vpc.outputs.subnet-a-id, data.terraform_remote_state.vpc.outputs.subnet-b-id, data.terraform_remote_state.vpc.outputs.subnet-d-id]
    }

    resources {
      memory = 2
      cores  = 2
      core_fraction = 50 # Использование процессорного времени - 50%
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = true # Включаем прерываемость машин
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 3
    }
  }

  allocation_policy {
    location {
        zone      = data.terraform_remote_state.vpc.outputs.subnet-a-zone
    }

    location {
        zone      = data.terraform_remote_state.vpc.outputs.subnet-b-zone
    }

    location {
        zone      = data.terraform_remote_state.vpc.outputs.subnet-d-zone
    }
  }

### Политики тех. работ и логирования

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "monday"
      start_time = "18:00"
      duration   = "3h"
    }

    maintenance_window {
      day        = "friday"
      start_time = "18:00"
      duration   = "4h30m"
    }
  }
}

################################

### Создаем статический ключ доступа

resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = "aje8onq253cceb5vui2n"
  description        = "static access key for view storage"
}

################################


## Блок даты с Virtual Private Cloud

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "stadeoff-s3"
    endpoint = "https://storage.yandexcloud.net"
    key    = "vpc/terraform.tfstate"
    region = "ru-central1"
    access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
    secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

### Блок даты с kub-master

data "terraform_remote_state" "kub-master" {
  backend = "s3"

  config = {
    bucket = "stadeoff-s3"
    endpoint = "https://storage.yandexcloud.net"
    key    = "kub-master/terraform.tfstate"
    region = "ru-central1"
    access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
    secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
    skip_region_validation      = true
    skip_credentials_validation = true
  }
}