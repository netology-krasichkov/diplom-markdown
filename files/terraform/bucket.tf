### Указываем яндекс провайдера и s3 backend

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoint = "https://storage.yandexcloud.net"
    bucket = "stadeoff-s3"
    region = "ru-central1"
    key    = "backend/terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  zone = "ru-central1-a"                          ## Зона доступности по умолчанию
  service_account_key_file = file("./key.json")
  folder_id = var.folder_id
}
################################

### Создаем сервисный аккаунт

resource "yandex_iam_service_account" "sa" {
  name = "sa-bucket"
}

################################

### Назначаем роль для сервисного аккаунта 

resource "yandex_resourcemanager_folder_iam_member" "sa-bucket" {
  folder_id = var.folder_id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

################################

### Создаем статический ключ доступа

resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = yandex_iam_service_account.sa.id
  description        = "static access key for object storage"
}

################################


### Создаем s3 bucket

resource "yandex_storage_bucket" "stadeoff-s3" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket     = var.s3_bucket_name
}