terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoint = "https://storage.yandexcloud.net"
    bucket = "stadeoff-s3"
    region = "ru-central1"
    key    = "sa-list/terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  zone = "ru-central1-a"                                                    ## Зона доступности по умолчанию
  service_account_key_file = file("./key.json")
  folder_id = var.folder_id
}
################################

### Создание сервисного аккаунта для управления k8s

resource "yandex_iam_service_account" "sa-k8s" {
  name = "sa-k8s"
}

################################

### Назначаем роли для сервисного аккаунта 

resource "yandex_resourcemanager_folder_iam_member" "k8s-clusters-agent" {
  folder_id = var.folder_id
  role      = "k8s.clusters.agent"
  member    = "serviceAccount:${yandex_iam_service_account.sa-k8s.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "vpc-publicAdmin" {
  folder_id = var.folder_id
  role      = "vpc.publicAdmin"
  member    = "serviceAccount:${yandex_iam_service_account.sa-k8s.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "container-registry-images-puller" {
  folder_id = var.folder_id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.sa-k8s.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "kms-keys-encrypterDecrypter" {
  folder_id = var.folder_id
  role      = "kms.keys.encrypterDecrypter"
  member    = "serviceAccount:${yandex_iam_service_account.sa-k8s.id}"
}

################################

### Создаем статический ключ доступа

resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = yandex_iam_service_account.sa-k8s.id
  description        = "static access key for k8s"
}

################################

### Output

output "sa-k8s-id" {
  value = yandex_iam_service_account.sa-k8s.id
}