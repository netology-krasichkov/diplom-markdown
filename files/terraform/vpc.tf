### Указываем яндекс провайдера и s3 backend

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoint = "https://storage.yandexcloud.net"
    bucket = "stadeoff-s3"
    region = "ru-central1"
    key    = "vpc/terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  zone = "ru-central1-a"                                                    ## Зона доступности по умолчанию
  service_account_key_file = file("./key.json")
  folder_id = var.folder_id
}
################################

### Создаем сетку 
resource "yandex_vpc_network" "netology_vpc" {
  name  = "netology-vpc"
}

### Создаем подсети в 3х разных зонах доступности

resource "yandex_vpc_subnet" "subnet-a" {
  name           = "subnet-a"
  network_id     = yandex_vpc_network.netology_vpc.id
  zone           = "ru-central1-a"
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_vpc_subnet" "subnet-b" {
  name           = "subnet-b"
  network_id     = yandex_vpc_network.netology_vpc.id
  zone           = "ru-central1-b"
  v4_cidr_blocks = ["192.168.20.0/24"]
}

resource "yandex_vpc_subnet" "subnet-d" {
  name           = "subnet-d"
  network_id     = yandex_vpc_network.netology_vpc.id
  zone           = "ru-central1-d"
  v4_cidr_blocks = ["192.168.30.0/24"]
}

################################

