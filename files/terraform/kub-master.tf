terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoint = "https://storage.yandexcloud.net"
    bucket = "stadeoff-s3"
    region = "ru-central1"
    key    = "kub-master/terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  zone = "ru-central1-a"                                                    ## Зона доступности по умолчанию
  service_account_key_file = file("./key.json")
  folder_id = var.folder_id
}
################################

resource "yandex_kubernetes_cluster" "stade-cluster-regional" {
  name        = "stade-cluster"
  description = "Cluster for diplom krasichkovav"

  network_id = data.terraform_remote_state.vpc.outputs.netology_vpc_id

### Cоздание регионального мастера

  master {
    regional {
      region = "ru-central1"

      location {
        zone      = data.terraform_remote_state.vpc.outputs.subnet-a-zone
        subnet_id = data.terraform_remote_state.vpc.outputs.subnet-a-id
      }

      location {
        zone      = data.terraform_remote_state.vpc.outputs.subnet-b-zone
        subnet_id = data.terraform_remote_state.vpc.outputs.subnet-b-id
      }

      location {
        zone      = data.terraform_remote_state.vpc.outputs.subnet-d-zone
        subnet_id = data.terraform_remote_state.vpc.outputs.subnet-d-id
      }
    }


    public_ip = true


### Политики тех. работ и логирования

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        day        = "monday"
        start_time = "18:00"
        duration   = "3h"
      }

      maintenance_window {
        day        = "friday"
        start_time = "18:00"
        duration   = "4h30m"
      }
    }

    master_logging {
      enabled = true
      folder_id = var.folder_id
      kube_apiserver_enabled = true
      cluster_autoscaler_enabled = true
      events_enabled = true
      audit_enabled = true
    }
  }
  
#################################


  service_account_id      = "aje8onq253cceb5vui2n"
  node_service_account_id = "aje8onq253cceb5vui2n"

  labels = {
    type       = "master"
    company    = "netology"
  }

  release_channel = "STABLE"
}

################################

### Создаем статический ключ доступа

resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = "aje8onq253cceb5vui2n"
  description        = "static access key for view storage"
}

################################


## Блок даты с Virtual Private Cloud

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "stadeoff-s3"
    endpoint = "https://storage.yandexcloud.net"
    key    = "vpc/terraform.tfstate"
    region = "ru-central1"
    access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
    secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

## Блок даты с sa-list

data "terraform_remote_state" "sa-list" {
  backend = "s3"

  config = {
    bucket = "stadeoff-s3"
    endpoint = "https://storage.yandexcloud.net"
    key    = "sa-list/terraform.tfstate"
    region = "ru-central1"
    access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
    secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
    skip_region_validation      = true
    skip_credentials_validation = true
  }
}